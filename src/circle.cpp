
#include <iostream>
using namespace std;

class Circle
{
public:
  double Diameter;
  double Radius;
  double Area;
  double Circumference;

  void PrintCircleStats()
  {
    cout << "Circle" << endl;
    cout << "Diameter: " << Diameter << endl;
    cout << "Radius: " << Radius << endl;
    cout << "Area: " << Area << endl;
    cout << "Circumference: " << Circumference << endl;
  }
};

int main() {
	Circle* a = new Circle();
	a->Diameter = 2;
	a->Radius = a->Diameter / 2;
	a->Area = a->Radius * a->Radius * 3.141592654;
	a->Circumference = a->Diameter *  3.141592654;
	a->PrintCircleStats();
	cout << endl;
	a->Diameter = 4;
	a->Radius = a->Diameter / 2;
	a->Area = a->Radius * a->Radius * 3.141592654;
	a->Circumference = a->Diameter *  3.141592654;
	a->PrintCircleStats();
}
